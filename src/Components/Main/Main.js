import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Page1 from '../../Pages/Page1/Page1'
import Bitfinex from '../../Pages/Bitfinex/Bitfinex'
import GDAX from '../../Pages/GDAX/GDAX'

const Main = () => (
    <Switch>
        <Route exact path='/' component={Page1}/>
        <Route path='/bitfinex' component={Bitfinex}/>
        <Route path='/gdax' component={GDAX}/>
    </Switch>
)

export default Main
