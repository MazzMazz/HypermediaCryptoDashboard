import React, { Component } from 'react';
import './Nav.scss';

import { Link } from 'react-router-dom'

class Nav extends Component {
    render() {
        return (
            <nav>
                <div>
                    <ul>
                        <li><Link to='/'> Home </Link></li>
                        <li><Link to='/bitfinex'>Bitfinex</Link></li>
                        <li><Link to='/gdax'>GDAX</Link></li>
                    </ul>
                </div>
            </nav>
        );
    }
}

export default Nav;