import React, { Component } from 'react';
import './App.scss';
import Nav from '../Components/Nav/Nav'
import Main from '../Components/Main/Main'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header>
          <Nav />
        </header>
        <Main />
      </div>
    );
  }
}

export default App;
