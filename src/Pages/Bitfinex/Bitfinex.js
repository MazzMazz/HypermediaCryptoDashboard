import React, { Component } from 'react'
import { Col, Card, Row } from 'react-materialize'
import tradePairs from './bitfinexTradePairs'
import './Bitfinex.scss'

class Bitfinex extends Component {

  constructor () {
    super()
    this.state = {
      lastUpdate: new Date(),
      subscriptions: [],
    }

  }

  componentDidMount () {
    this.connection = new WebSocket('wss://api.bitfinex.com/ws/2')
    // listen to onmessage event
    this.connection.onmessage = message => {
      // add the new message to state
      this.handleEvent(message.data)
      this.setState({lastUpdate: new Date()})
    }
    let symbols = tradePairs.slice(0,30);
    this.connection.onopen = () => {
      for (let i = 0; i < symbols.length; i++) {
        this.connection.send(JSON.stringify({
          event: 'subscribe',
          channel: 'trades',
          symbol: symbols[i],
        }))
      }
    }
  }

  componentWillUnmount () {
    this.connection.close();
  }

  handleEvent (data) {
    data = JSON.parse(data)
    switch (data.event) {
      case 'subscribed':
        let subscriptions = this.state.subscriptions
        // Handle new subscriptions

        subscriptions[data.chanId] = {
          channelId: data.chanId,
          key: data.chanId,
          pair: data.pair,
          symbol: data.symbol,
          hint: 'Waiting for new trades!',
          trades: [],
        }

        this.setState({subscriptions: subscriptions})

        break
      default:
        // Heartbeat to keep connection alive
        if (data[1] !== 'hb' && data[1] === 'tu') {
          if (!this.state.subscriptions[data[0]]) {
            return
          }

          let subs = this.state.subscriptions

          let currentSub = subs[data[0]]
          currentSub.trades.unshift({
            type: data[2][2] < 0 ? 'Sold' : 'Bought',
            amount: data[2][2] < 0 ? (data[2][2] * -1) : data[2][2],
            unitPrice: data[2][3],
          })
          currentSub.trades = currentSub.trades.slice(0, 5)
          currentSub.hint = ''

          subs[data[0]] = currentSub

          this.setState({subscriptions: subs})
        }

        break
    }
  }

  render () {
    return (
      <div className="Bitfinex">
        <Row>
          Last Update: {this.state.lastUpdate.toISOString()}
        </Row>
        <Row>
          {
            this.state.subscriptions.map((obj, index) => {
              return <Col l={3} m={4} s={12}>
                <Card className='blue-grey darken-1' textClassName='white-text'
                      title={obj.pair} key={index + 'card'}
                      style={{height: '240px'}}>
                  <hr/>
                  <div>
                    {obj.hint}
                    {

                      obj.trades.map((trade, tradeIndex) => {
                        return <div key={index + '_' +
                        tradeIndex}>{trade.type} {trade.amount} at {trade.unitPrice} </div>
                      })
                    }
                  </div>
                </Card>
              </Col>
            })
          }
        </Row>
      </div>
    )
  }
}

export default Bitfinex
