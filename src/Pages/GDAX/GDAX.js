import React, { Component } from 'react'
import { Col, Card, Row } from 'react-materialize'
import tradePairs from './gdaxTradePairs'
import './GDAX.scss'

class GDAX extends Component {

    constructor() {
        super();
        this.state = {
            lastUpdate: new Date(),
            subscriptions: [],
        }

    }

    componentDidMount() {
        this.connection = new WebSocket('wss://ws-feed.gdax.com/');
        // listen to onmessage event
        this.connection.onmessage = message => {
            // add the new message to state
            this.handleEvent(message.data);
            this.setState({lastUpdate: new Date()})
        };
        let symbols = tradePairs;
        this.connection.onopen = () => {
            for (let i = 0; i < symbols.length; i++) {
                this.connection.send(JSON.stringify({
                    type: 'subscribe',
                    product_ids: symbols,
                    channels: [
                        "level2",
                    ]
                }));
            }
        }
    }

    componentWillUnmount() {
        this.connection.close();
    }

    handleEvent(data) {
        data = JSON.parse(data);
        switch (data.type) {
            case 'subscriptions':
                let subscriptions = this.state.subscriptions;
                // Handle new subscriptions

                for( let i = 0; i < data.channels[0].product_ids.length; i++){
                    subscriptions[i] = {
                        channelId: data.channels[0].product_ids[i],
                        key:  data.channels[0].product_ids[i],
                        pair: data.channels[0].product_ids[i],
                        symbol: data.channels[0].product_ids[i],
                        hint: 'Waiting for new trades!',
                        trades: [],
                    };
                };

                this.setState({subscriptions: subscriptions});

                break;
            default:
                // Heartbeat to keep connection alive

                if (data.type !== 'heartbeat' && data.type === 'l2update') {

                    let index;
                    for(let i = 0; i < this.state.subscriptions.length; i++) {
                        if(this.state.subscriptions[i].channelId === data.product_id){
                           index = i;
                        }
                    }
                    if (!this.state.subscriptions[index]) {
                        return
                    }

                    let subs = this.state.subscriptions;

                    let currentSub = subs[index];
                    currentSub.trades.unshift({
                        type: data.changes[0][0],
                        amount: data.changes[0][2],
                        unitPrice: data.changes[0][1],
                    });
                    currentSub.trades = currentSub.trades.slice(0, 5);
                    currentSub.hint = '';

                    subs[index] = currentSub;

                    this.setState({subscriptions: subs})
                }

                break
        }
    }

    render() {
        return (
            <div className="GDAX">
                <Row>
                    Last Update: {this.state.lastUpdate.toISOString()}
                </Row>
                <Row>

                    {
                        this.state.subscriptions.map((obj, index) => {
                            return <Col l={3} m={4} s={12}>
                                <Card className='blue-grey darken-1' textClassName='white-text'
                                      title={obj.pair} key={index + 'card'}
                                      style={{height: '240px'}}>
                                    <hr/>
                                    <div>
                                        hello
                                        {obj.hint}
                                        {
                                            obj.trades.map((trade, tradeIndex) => {
                                                return <div key={index + '_' +
                                                tradeIndex}>{trade.type} {trade.amount} at {trade.unitPrice} </div>
                                            })
                                        }
                                    </div>
                                </Card>
                            </Col>
                        })
                    }
                </Row>
            </div>
        )
    }
}

export default GDAX
