import React, { Component } from 'react';
import { Col,  Row } from 'react-materialize';
import './Page1.scss';
import bgImage from './images/bg-image.jpg';
import mazz from './images/mazz.jpg';
import woif from './images/woif.jpg';
import bitfinex from './images/bitfinex.svg';
import gdax from './images/gdax.svg';

class Page1 extends Component {
    render() {
        return (
            <div className="App">
                <Row>
                    <Col className="page1">
                        <div class="bg-pic">
                            <img alt="background" src={bgImage}/>
                        </div>
                    </Col>

                    <Col s={12} m={12} l={10} offset={"l1"} className="welcome">
                        <Col s={12} m={12} l={7} className="hello">
                            <Col s={12} m={12} l={12}>
                                <div className="hello-content">
                                    <h1>Welcome to cryptoSocket</h1>
                                    <h3>realtime trading data from cryptoexchanges</h3>
                                    <p>We are a platform for displaying realtime trades on cryptoexchanges via socket
                                        connection. Catch a glimpse of today's cryptomarket and see if your favourite cryptocurrency
                                        is trending in exchanges.</p>
                                </div>

                                <div className="what">
                                    <h4>What are we doing in this project?</h4>
                                    <p>We are connecting our application with public api endpoints from cryptoexchanges
                                    to present current realtime trading data. On display are the four tradingpairs with the
                                    highest frequency of trades - in case of Bitfinex the trading pairs are "btcusd", "ethusd", "eosusd" and "bchusd"
                                    </p>
                                </div>
                            </Col>
                            <Col s={12} m={12} l={12}>
                                <div className="exchanges">
                                    <h3>Currently connected exchanges:</h3>

                                    <a href="/bitfinex">
                                        <Col s={12} m={12} l={5} className="exchange">
                                            <img src={bitfinex} alt="bitfinex exchange ticker"
                                                 className="exchange__image exchange__image--wide"/>
                                        </Col>
                                    </a>
                                    <a href="/gdax">
                                        <Col s={12} m={12} l={5} offset={"l1"} className="exchange">
                                            <img src={gdax} alt="gdax exchange ticker" className="exchange__image"/>
                                        </Col>
                                    </a>
                                </div>
                            </Col>
                        </Col>
                        <Col s={12} m={12} l={5} className="team">
                            <h3>Our Team</h3>
                            <Col s={12} m={12} l={6} className="teamMember teamMember--left">
                                <div className="teamMember-content">
                                    <img src={mazz} alt="Our genious mind - Mathias Maier"/>
                                    <p className="team-member">Mathias Maier</p>
                                </div>
                            </Col>
                            <Col s={12} m={12} l={6} className="teamMember teamMember--right">
                                <div className="teamMember-content">
                                    <img src={woif} alt="Our second genius mind- Wolfgang Eßl"/>
                                    <p className="team-member">Wolfgang Eßl</p>
                                </div>
                            </Col>
                            <Col s={12} m={12} l={12} className="contact">
                                <div className="contact-content">
                                    <h4>Contact us!</h4>
                                    <p>You have questions about our project? You want to extend or connect further exchanges?
                                        We would love to help you and give you access to our open-source project!
                                    </p>
                                    <p><a href="mailto:S1710629008@students.fh-hagenberg.at">Contact us here!</a></p>
                                </div>
                            </Col>
                        </Col>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default Page1;
